<?php

namespace Context;

use Behat\MinkExtension\Context\MinkContext;
use function peterpostmann\uri\fileuri;

class FeatureContext extends MinkContext {

    public function setMinkParameters(array $parameters) {
        $parameters['base_url'] = fileuri(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'html_files');
        parent::setMinkParameters($parameters);
    }
}
